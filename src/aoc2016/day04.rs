use regex::Regex;

fn parse_input(input_str: &str) {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    let re = Regex::new(r"(?P<name>[^\-\s\[\]]+)-|(?P<sector_id>\d+)|\[(?P<checksum>[a-z]+)\]").unwrap();
    // let (name, id, checksum) = ();
    re.captures(&input).unwrap();
}

pub fn part1() -> String {
    let _input = parse_input("./input/2016/day04.txt");

    String::new()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2016/day04.txt");

    String::new()
}