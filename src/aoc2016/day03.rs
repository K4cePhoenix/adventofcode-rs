struct Vertex {
    x: u32,
    y: u32,
    z: u32,
}

impl Vertex {
    fn sum(&self) -> u32 {
        self.x + self.y + self.z
    }

    fn max(&self) -> u32 {
        if self.x > self.y && self.x > self.z {
            self.x
        } else if self.y > self.z {
            self.y
        } else {
            self.z
        }
    }
}

fn parse_input(input_str: &str) -> Vec<Vertex> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|l| {
            let tmp = String::from(l);
            let mut v: Vec<&str> = tmp.split_whitespace().collect();
            Vertex { x: v.remove(0).parse().unwrap(),
                y: v.remove(0).parse().unwrap(),
                z: v.remove(0).parse().unwrap(),
            }
        }).collect()
}

pub fn part1() -> String {
    let input: Vec<Vertex> = parse_input("./input/2016/day03.txt");
    let mut possible_triangles: u32 = 0;
    for vertex in input {
        if vertex.sum()-vertex.max() > vertex.max() {
            possible_triangles += 1;
        }
    }

    possible_triangles.to_string()
}

pub fn part2() -> String {
    let input: Vec<Vertex> = parse_input("./input/2016/day03.txt");
    let mut possible_triangles: u32 = 0;
    let mut v: Vec<Vertex> = Vec::new();
    for i in (0..input.len()).step_by(3) {
        v.push(Vertex {
            x: input.get(i).unwrap().x,
            y: input.get(i+1).unwrap().x,
            z: input.get(i+2).unwrap().x,
        });
        v.push(Vertex {
            x: input.get(i).unwrap().y,
            y: input.get(i+1).unwrap().y,
            z: input.get(i+2).unwrap().y,
        });
        v.push(Vertex {
            x: input.get(i).unwrap().z,
            y: input.get(i+1).unwrap().z,
            z: input.get(i+2).unwrap().z,
        });
    }

    for vertex in v {
        if vertex.sum()-vertex.max() > vertex.max() {
            possible_triangles += 1;
        }
    }

    possible_triangles.to_string()
}