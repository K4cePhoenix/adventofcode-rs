use crypto::{
    md5::Md5,
    digest::Digest,
};

fn parse_input(input_str: &str) -> String {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    String::from(input.trim())
}

pub fn part1() -> String {
    let input = parse_input("./input/2016/day05.txt");
    let mut hasher = Md5::new();
    let mut code = String::new();
    for i in 0..std::u32::MAX {
        if code.len() < 8 {
            hasher.input_str(&input);
            hasher.input_str(&i.to_string());
            let hash = hasher.result_str();
            hasher.reset();
            match &hash[0..5] {
                "00000" => code.push(hash.chars().nth(5).unwrap()),
                _ => ()
            }
        }
        else { break; }
    }

    code.to_uppercase()
}

pub fn part2() -> String {
    let input = parse_input("./input/2016/day05.txt");
    let mut hasher = Md5::new();
    let mut code = String::from("________");
    let mut chars_found = 0;
    for i in 0..std::u32::MAX {
        if chars_found < 8 {
            hasher.input_str(&input);
            hasher.input_str(&i.to_string());
            let hash = hasher.result_str();
            hasher.reset();
            match &hash[0..5] {
                "00000" => {
                    let (pos, res) = (hash.chars().nth(5).unwrap().to_digit(10), hash.chars().nth(6));
                    println!("{}", &hash[0..7]);
                    match pos {
                        Some(pos2) => {
                            if pos2 < 8 && code.chars().nth(pos2 as usize).unwrap() == '_' {
                                code.insert(pos2 as usize, res.unwrap());
                                code.remove((pos2+1) as usize);
                                chars_found += 1;
                                println!("{}", code);
                            }
                        },
                        None => (),
                    }
                },
                _ => ()
            }
        }
        else { break; }
    }

    code.to_uppercase()
}
