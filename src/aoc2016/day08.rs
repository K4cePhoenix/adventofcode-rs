

type Display = Vec<Vec<bool>>;

struct Instruction {
    op: String,
    a: usize,
    b: usize,
}

fn parse_input(input_str: &str) -> Vec<Instruction> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|l| {
            let mut op = l.split(' ').nth(0).unwrap();
            let a: usize;
            let b: usize;
            if op == "rect" {
                a = l.split(' ').nth(1).unwrap().split('x').nth(0).unwrap().parse::<usize>().unwrap();
                b = l.split(' ').nth(1).unwrap().split('x').nth(1).unwrap().parse::<usize>().unwrap();
            } else {
                op = l.split(' ').nth(1).unwrap();
                a = l.split(" by ").nth(0).unwrap().split('=').nth(1).unwrap().parse::<usize>().unwrap();
                b = l.split(" by ").nth(1).unwrap().parse::<usize>().unwrap();
            }
            Instruction { op: String::from(op), a: a, b: b }
        }).collect()
}

fn build_display(disp: &mut Display) {
    for _ in 0..6 {
        let mut row: Vec<bool> = Vec::new();
        for _ in 0..50 {
            row.push(false);            
        }
        disp.push(row);
    }
}

fn _print_display(disp: &Display) {
    for i in 0..disp.len() {
        for j in 0..disp.get(0).unwrap().len() {
            if *disp.get(i).unwrap().get(j).unwrap() {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!("");
    }
}

pub fn part1() -> String {
    let input = parse_input("./input/2016/day08.txt");
    let mut disp: Display = Vec::new();
    build_display(&mut disp);
    for instruction in input {
        if instruction.op == "rect" {
            for i in 0..instruction.b {
                for j in 0..instruction.a {
                    let row = disp.iter_mut().nth(i).unwrap();
                    let el = row.iter_mut().nth(j).unwrap();
                    *el = true;
                }
            }
        } else if instruction.op == "row" {

        } else if instruction.op == "column" {

        }
        // print_display(&disp);
    }

    String::new()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2016/day08.txt");
    
    String::new()
}
