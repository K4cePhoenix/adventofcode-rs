

fn parse_input(input_str: &str) -> Vec<String> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|l| String::from(l))
        .collect()
}

pub fn part1() -> String {
    let input = parse_input("./input/2016/day07.txt");
    let mut valid_ips = 0;
    let mut hnet_seq;
    let mut valid;
    for line in input.iter() {
        hnet_seq = false;
        valid = false;
        for i in 0..(line.len()-3) {
            let mut chars = line[i..].chars().take(4);
            let c = (chars.next().unwrap(), chars.next().unwrap(), chars.next().unwrap(), chars.next().unwrap());
            match c.0 {
                '[' => hnet_seq = true,
                ']' => hnet_seq = false,
                _ if !hnet_seq && c.0 == c.3 && c.1 == c.2 && c.0 != c.1 => valid = true,
                _ => (),
            }
            if hnet_seq { valid = false; break; }
        }
        if valid {
            valid_ips += 1;
        }
    }

    valid_ips.to_string()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2016/day07.txt");
    
    String::new()
}
