

fn parse_input(input_str: &str) -> String {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input.trim().to_string()
}

struct Sequence {
    string: String,
    repetitions: u32,
}

pub fn part1() -> String {
    let input = parse_input("./input/2016/day09.txt");
    let mut res = Vec::new();
    let mut skips = 0;
    for i in 0..input.len() {
        if input.chars().nth(i).unwrap() == '(' && i < input.len()-6 {
            let mut j = 0;
            while input.chars().nth(i+j).unwrap() != 'x' {
                j += 1;
            }
            let mut k = 0;
            while input.chars().nth(i+j+1+k).unwrap() != ')' {
                k += 1;
            }
            // println!("{:?}; {:?}", &input[i+1..i+j], &input[i+j+1..i+j+k+1]);
            let length = &input[i+1..i+j].parse::<usize>().unwrap();
            let repeats = &input[i+j+1..i+j+k+1].parse::<usize>().unwrap();
            let seq = Sequence {
                string: String::from(&input[i+j+k+2..i+j+k+2+length]),
                repetitions: *repeats as u32,
            };
            let mut pbs = String::new();
            for _ in 0..seq.repetitions {
                pbs.push_str(&seq.string);
            }
            res.push(pbs);
            skips = j+k+1;
        } else if skips > 0 {
            // println!("{}", &input.chars().nth(i).unwrap());
            skips -= 1;
        } else {
            res.push(String::from(&input.chars().nth(i).unwrap().to_string()));
        }
    }

    res.join("").len().to_string()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2016/day09.txt");
    
    String::new()
}
