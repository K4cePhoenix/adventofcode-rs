use std::collections::HashSet;

enum Direction {
    North,
    South,
    East,
    West,
}

pub enum DirectionChange {
    Left,
    Right,
}

pub fn parse_input(input_str: &str) -> Vec<(DirectionChange, i32)> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .split(", ")
        .map(
            |d| (
                match d.chars().nth(0).unwrap() {
                    'R' => DirectionChange::Right,
                    'L' => DirectionChange::Left,
                    _ => unreachable!(),
                },
                d.chars().nth(1).unwrap().to_digit(10).unwrap() as i32
            )
        )
        .collect()
}

pub fn part1() -> u32 {
    let input: Vec<(DirectionChange, i32)> = parse_input("./input/2016/day01.txt");
    let mut current_dir = Direction::North;
    let mut x: i32 = 0;
    let mut y: i32 = 0;
    for i in input.iter() {
        match (&i.0, current_dir) {
            (DirectionChange::Right, Direction::North) => { current_dir = Direction::East; x += i.1; },
            (DirectionChange::Right, Direction::East) => { current_dir = Direction::South; y -= i.1; },
            (DirectionChange::Right, Direction::South) => { current_dir = Direction::West; x -= i.1; },
            (DirectionChange::Right, Direction::West) => { current_dir = Direction::North; y += i.1; },

            (DirectionChange::Left, Direction::North) => { current_dir = Direction::West; x -= i.1; },
            (DirectionChange::Left, Direction::East) => { current_dir = Direction::North; y += i.1; },
            (DirectionChange::Left, Direction::South) => { current_dir = Direction::East; x += i.1; },
            (DirectionChange::Left, Direction::West) => { current_dir = Direction::South; y -= i.1; },
        };
    }

    (x.abs() + y.abs()) as u32
}

pub fn part2() -> u32 {
    let input: Vec<(DirectionChange, i32)> = parse_input("./input/2016/day01.txt");
    let mut current_dir = Direction::North;
    let mut x: i32 = 0;
    let mut y: i32 = 0;
    let mut v: HashSet<(i32, i32)> = HashSet::new();
    for i in input.iter() {
        match (&i.0, current_dir) {
            (DirectionChange::Right, Direction::North) => { current_dir = Direction::East; x += i.1; },
            (DirectionChange::Right, Direction::East) => { current_dir = Direction::South; y -= i.1; },
            (DirectionChange::Right, Direction::South) => { current_dir = Direction::West; x -= i.1; },
            (DirectionChange::Right, Direction::West) => { current_dir = Direction::North; y += i.1; },

            (DirectionChange::Left, Direction::North) => { current_dir = Direction::West; x -= i.1; },
            (DirectionChange::Left, Direction::East) => { current_dir = Direction::North; y += i.1; },
            (DirectionChange::Left, Direction::South) => { current_dir = Direction::East; x += i.1; },
            (DirectionChange::Left, Direction::West) => { current_dir = Direction::South; y -= i.1; },
        };

        if v.iter().any(|&(a, b)| a == x && b == y) { break; }
        else { v.insert((x, y)); }
    }

    (x.abs() + y.abs()) as u32
}