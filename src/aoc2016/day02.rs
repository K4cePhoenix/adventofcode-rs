enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn parse_input(input_str: &str) -> Vec<Vec<Direction>> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|s|
            s.chars()
                .map(|c| match c {
                    'U' => Direction::Up,
                    'D' => Direction::Down,
                    'R' => Direction::Right,
                    'L' => Direction::Left,
                    _ => unreachable!(),
                })
                .collect::<Vec<Direction>>()
        )
        .collect::<Vec<Vec<Direction>>>()
}

pub fn part1() -> String {
    let input: Vec<Vec<Direction>> = parse_input("./input/2016/day02.txt");
    let mut code = String::new();
    for i in input.iter() {
        let mut _pos = 5;
        for j in i.iter() {
            match j {
                Direction::Up if _pos > 3 => _pos -= 3,
                Direction::Down if _pos < 7 => _pos += 3,
                Direction::Right if _pos % 3 != 0 => _pos += 1,
                Direction::Left if _pos % 3 != 1 => _pos -= 1,
                _ => (),
            }
        }
        code.push(std::char::from_digit(_pos, 10).unwrap());
    }

    code
}

pub fn part2() -> String {
    let input: Vec<Vec<Direction>> = parse_input("./input/2016/day02.txt");
    let mut code = String::new();
    let top_bounds = [5, 2, 1, 4, 9];
    let bottom_bounds = [5, 10, 13, 12, 9];
    let left_bounds = [1, 2, 5, 10, 13];
    let right_bounds = [1, 4, 9, 12, 13];
    for i in input.iter() {
        let mut _pos = 5;
        for j in i.iter() {
            match j {
                Direction::Up if !top_bounds.contains(&_pos) => match _pos {
                    3 => _pos -= 2,
                    13 => _pos -= 2,
                    _ => _pos -= 4,
                },
                Direction::Down if !bottom_bounds.contains(&_pos) => match _pos {
                    1 => _pos += 2,
                    11 => _pos += 2,
                    _ => _pos += 4,
                },
                Direction::Right if !right_bounds.contains(&_pos) => _pos += 1,
                Direction::Left if !left_bounds.contains(&_pos) => _pos -= 1,
                _ => (),
            }
        }
        code.push(std::char::from_digit(_pos, 16).unwrap());
    }

    code.to_ascii_uppercase()
}