

fn parse_input(input_str: &str) -> Vec<String> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|l| {
            String::from(l.trim())
        }).collect()
}

pub fn part1() -> String {
    let input = parse_input("./input/2016/day06.txt");
    let mut message = String::new();
    for i in 0..input.get(0).unwrap().len() {
        let mut v = Vec::new();
        for _ in 0..26 {
            v.push(0);
        }
        for s in input.iter() {
            let p = s.chars().nth(i).unwrap() as usize - 'a' as usize;
            println!("{}", p);
            let val = v.remove(p);
            v.insert(p, val + 1);
        }
        let m: &u8 = v.iter().max().unwrap();
        let mp = v.iter().position(|x| x == m).unwrap() as u8;
        let c = ('a' as u8 + mp) as char;
        message.push(c);
    }

    message.to_uppercase()
}

pub fn part2() -> String {
    let input = parse_input("./input/2016/day06.txt");
    let mut message = String::new();
    for i in 0..input.get(0).unwrap().len() {
        let mut v = Vec::new();
        for _ in 0..26 {
            v.push(0);
        }
        for s in input.iter() {
            let p = s.chars().nth(i).unwrap() as usize - 'a' as usize;
            let val = v.remove(p);
            v.insert(p, val + 1);
        }
        let m: &u8 = v.iter().min().unwrap();
        let mp = v.iter().position(|x| x == m).unwrap() as u8;
        let c = ('a' as u8 + mp) as char;
        message.push(c);
    }

    message.to_uppercase()
}
