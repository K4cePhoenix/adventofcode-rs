

fn parse_input(input_str: &str) -> Vec<[char; 2]> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input.lines()
        .map(|line| {
            let mut result: [char; 2] = ['G', 'G'];
            for c in line.split_ascii_whitespace() {
                match c.chars().nth(0) {
                    Some('A') => result[0] = 'A',
                    Some('B') => result[0] = 'B',
                    Some('C') => result[0] = 'C',
                    Some('X') => result[1] = 'X',
                    Some('Y') => result[1] = 'Y',
                    Some('Z') => result[1] = 'Z',
                    _ => unreachable!(),
                }
            }
            result
        })
        .collect()
}

pub fn part1() -> String {
    let input = parse_input("./input/2022/day02.txt");

    let mut score = 0;
    for round in input {
        match round[0] {
            'A' if  round[1] == 'X' => score += 1 + 3,
            'A' if  round[1] == 'Y' => score += 2 + 6,
            'A' if  round[1] == 'Z' => score += 3 + 0,
            'B' if  round[1] == 'X' => score += 1 + 0,
            'B' if  round[1] == 'Y' => score += 2 + 3,
            'B' if  round[1] == 'Z' => score += 3 + 6,
            'C' if  round[1] == 'X' => score += 1 + 6,
            'C' if  round[1] == 'Y' => score += 2 + 0,
            'C' if  round[1] == 'Z' => score += 3 + 3,
            _ => unreachable!(),
        }
    }

    score.to_string()
}

pub fn part2() -> String {
    let input = parse_input("./input/2022/day02.txt");

    let mut score = 0;
    for round in input {
        match round[0] {
            'A' if  round[1] == 'X' => score += 3,
            'A' if  round[1] == 'Y' => score += 1 + 3,
            'A' if  round[1] == 'Z' => score += 2 + 6,
            'B' if  round[1] == 'X' => score += 1 + 0,
            'B' if  round[1] == 'Y' => score += 2 + 3,
            'B' if  round[1] == 'Z' => score += 3 + 6,
            'C' if  round[1] == 'X' => score += 2 + 0,
            'C' if  round[1] == 'Y' => score += 3 + 3,
            'C' if  round[1] == 'Z' => score += 1 + 6,
            _ => unreachable!(),
        }
    }

    score.to_string()
}
