

fn parse_input(input_str: &str) -> () {
    let _input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
}

pub fn part1() -> String {
    let _input = parse_input("./input/2022/day03.txt");

    String::new()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2022/day03.txt");
    
    String::new()
}
