use std::collections::VecDeque;



fn parse_input(input_str: &str) -> Vec<char> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input.chars().collect()
}

fn check_for_marker(queue: VecDeque<char>, capacity: usize) -> bool {
    for i in 0..capacity {
        for j in i+1..capacity {
            if queue.get(i).unwrap() == queue.get(j).unwrap() {
                return false;
            }
        }
    }
    true
}

pub fn part1() -> String {
    let input = parse_input("./input/2022/day06.txt");
    let init_queue = ['-', '-', '-', '-'];
    let mut queue: VecDeque<char> = VecDeque::from(init_queue);

    for (i, &c) in input.iter().enumerate() {
        queue.pop_front();
        queue.push_back(c);
        if queue.contains(&'-') { continue; }

        if check_for_marker(queue.clone(), init_queue.len()) {
            return (i+1).to_string()
        }
    }

    unreachable!()
}

pub fn part2() -> String {
    let input = parse_input("./input/2022/day06.txt");
    let init_queue = ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'];
    let mut queue: VecDeque<char> = VecDeque::from(init_queue);

    for (i, &c) in input.iter().enumerate() {
        queue.pop_front();
        queue.push_back(c);
        if queue.contains(&'-') { continue; }

        if check_for_marker(queue.clone(), init_queue.len()) {
            return (i+1).to_string()
        }
    }

    unreachable!()
}
