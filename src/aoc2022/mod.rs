pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;
pub mod day05;
pub mod day06;
pub mod day07;
// pub mod day08;
// pub mod day09;
// pub mod day10;
// pub mod day11;
// pub mod day12;
// pub mod day13;
// pub mod day14;
// pub mod day15;
// pub mod day16;
// pub mod day17;
// pub mod day18;
// pub mod day19;
// pub mod day20;
// pub mod day21;
// pub mod day22;
// pub mod day23;
// pub mod day24;


pub fn run() -> () {
    println!("2022/01 P1: {}, P2: {}", day01::part1(), day01::part2());
    println!("2022/02 P1: {}, P2: {}", day02::part1(), day02::part2());
    println!("2022/03 P1: {}, P2: {}", day03::part1(), day03::part2());
    println!("2022/04 P1: {}, P2: {}", day04::part1(), day04::part2());
    println!("2022/05 P1: {}, P2: {}", day05::part1(), day05::part2());
    println!("2022/06 P1: {}, P2: {}", day06::part1(), day06::part2());
    println!("2022/07 P1: {}, P2: {}", day07::part1(), day07::part2());
}
