
fn parse_input(input_str: &str) -> Vec<char> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input.chars().collect()
}

pub fn part1() -> String {
    let _input = parse_input("./input/2022/day06.txt");
    
    String::new()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2022/day06.txt");
    
    String::new()
}
