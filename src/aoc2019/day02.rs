
const RAM_SIZE: usize = 2048;

fn parse_input(input_str: &str) -> Vec<u32> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .trim()
        .split(',')
        .map(|n| n.parse::<u32>().unwrap())
        .collect()
}

enum IPtrMove {
    _Step,
    Skip(u32),
    End,
}

struct IntcodeComputer {
    memory: [u32; RAM_SIZE],
    i_ptr: usize,
}

impl IntcodeComputer {
    fn new() -> Self {
        IntcodeComputer {
            memory: [0; RAM_SIZE],
            i_ptr: 0,
        }
    }

    fn reset(&mut self) {
        self.memory = [0; RAM_SIZE];
        self.i_ptr = 0;
    }

    fn load_program(&mut self, program: &Vec<u32>) {
        for (idx, &byte) in program.iter().enumerate() {
            self.memory[idx] = byte;
        }
    }

    fn run_program(&mut self) {
        loop {
            match self.execute_intcode() {
                IPtrMove::End => break,
                IPtrMove::_Step => self.i_ptr += 1,
                IPtrMove::Skip(n) => self.i_ptr += n as usize,
            }
        }
    }

    fn execute_intcode(&mut self) -> IPtrMove {
        match self.memory[self.i_ptr] {
            99 => IPtrMove::End,
            1 => {
                let a = self.memory[self.i_ptr+1];
                let b = self.memory[self.i_ptr+2];
                let c = self.memory[self.i_ptr+3];
                self.memory[c as usize] = self.memory[a as usize] + self.memory[b as usize];
                IPtrMove::Skip(4)
            },
            2 => {
                let a = self.memory[self.i_ptr+1];
                let b = self.memory[self.i_ptr+2];
                let c = self.memory[self.i_ptr+3];
                self.memory[c as usize] = (self.memory[a as usize] * self.memory[b as usize]) as u32;
                IPtrMove::Skip(4)
            },
            _ => unreachable!(),
        }
    }
}

pub fn part1() -> String {
    let input = parse_input("./input/2019/day02.txt");

    let mut computer = IntcodeComputer::new();
    computer.load_program(&input);
    computer.memory[1] = 12;
    computer.memory[2] = 2;
    computer.run_program();

    computer.memory[0].to_string()
}

pub fn part2() -> String {
    let input = parse_input("./input/2019/day02.txt");

    let mut computer = IntcodeComputer::new();
    for i in 0..100 {
        for j in 0..100 {
            computer.reset();
            computer.load_program(&input);
            computer.memory[1] = i;
            computer.memory[2] = j;
            computer.run_program();
            if computer.memory[0] == 19690720 {
                return (100 * i + j).to_string();
            }
        }
    }
    
    String::new()
}
