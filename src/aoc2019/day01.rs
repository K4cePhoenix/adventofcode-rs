

fn parse_input(input_str: &str) -> Vec<u64> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|n|
            match n.parse::<u64>() {
                Ok(x) => x,
                _ => unreachable!()
            }
        )
        .collect()
}

fn calc_fuel_req(module: u64) -> u64 {
    match (module as f64 / 3.).floor() as i64 - 2 > 0 {
        true => (module as f64 / 3.).floor() as u64 - 2,
        false => 0,
    }
}

fn calc_fuel_for_fuel_req(mut module: u64) -> u64 {
    let mut n = 0;
    while module > 0 {
        module = calc_fuel_req(module);
        n += module as u64;
    }
    n
}

pub fn part1() -> String {
    let input = parse_input("./input/2019/day01.txt");
    format!("{}", input.iter().map(|&n| calc_fuel_req(n)).sum::<u64>())
}

pub fn part2() -> String {
    let input = parse_input("./input/2019/day01.txt");
    format!("{}", input.iter().map(|&n| calc_fuel_for_fuel_req(n)).sum::<u64>())
}
