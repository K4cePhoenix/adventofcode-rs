extern crate regex;
extern crate crypto;

pub mod aoc2015;
pub mod aoc2016;
pub mod aoc2017;
pub mod aoc2018;
pub mod aoc2019;
pub mod aoc2022;

fn main() {
    // Advent of code 2015
    // aoc2015::run();

    // Advent of code 2016
    // aoc2016::run();

    // Advent of code 2017
    // aoc2017::run();

    // Advent of code 2018
    // aoc2018::run();

    // Advent of Code 2019
    // aoc2019::run();

    // Advent of Code 2022
    aoc2022::run();
}
