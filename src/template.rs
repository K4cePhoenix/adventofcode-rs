

fn parse_input(input_str: &str) -> () {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
}

pub fn part1() -> String {
    let input = parse_input("./input/20XX/dayXX.txt");

    String::new()
}

pub fn part2() -> String {
    let input = parse_input("./input/20XX/dayXX.txt");
    
    String::new()
}
