use crypto::{
    md5::Md5,
    digest::Digest,
};

fn parse_input(input_str: &str) -> String {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    String::from(input.trim())
}

pub fn part1() -> String {
    let input = parse_input("./input/2015/day04.txt");
    let mut hasher = Md5::new();
    let _code = String::new();
    for i in 0..std::u32::MAX {
        hasher.input_str(&input);
        hasher.input_str(&i.to_string());
        let hash = hasher.result_str();
        hasher.reset();
        match &hash[0..5] {
            "00000" => return i.to_string(),
            _ => ()
        }
    }

    unreachable!()
}

pub fn part2() -> String {
    let input = parse_input("./input/2015/day04.txt");
    let mut hasher = Md5::new();
    let _code = String::new();
    for i in 0..std::u32::MAX {
        hasher.input_str(&input);
        hasher.input_str(&i.to_string());
        let hash = hasher.result_str();
        hasher.reset();
        match &hash[0..6] {
            "000000" => return i.to_string(),
            _ => ()
        }
    }

    unreachable!()
}
