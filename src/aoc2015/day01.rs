

fn parse_input(input_str: &str) -> String {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input.to_string()
}

pub fn part1() -> String {
    let input = parse_input("./input/2015/day01.txt");
    let mut floor = 0;
    for c in input.chars() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => (),
        }
    }

    floor.to_string()
}

pub fn part2() -> String {
    let input = parse_input("./input/2015/day01.txt");
    let mut floor = 0;
    for i in 0..input.len() {
        match input.get(i..i+1).unwrap() {
            "(" => floor += 1,
            ")" => floor -= 1,
            _ => (),
        }
        if floor == -1 {
            return (i+1).to_string();
        }
    }

    String::from("-1")
}
