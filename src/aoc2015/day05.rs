

fn parse_input(input_str: &str) -> Vec<String> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|l| {
            String::from(l.trim())
        }).collect()
}

pub fn part1() -> String {
    let _input = parse_input("./input/2015/day05.txt");

    String::new()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2015/day05.txt");
    
    String::new()
}
