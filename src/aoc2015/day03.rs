use std::collections::HashSet;

fn parse_input(input_str: &str) -> String {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    
    input
}

pub fn part1() -> String {
    let input = parse_input("./input/2015/day03.txt");
    let pos: Vec<i32> = vec![0, 0];
    let mut visited: HashSet<&Vec<i32>> = HashSet::new();
    visited.insert(&pos);
    for _c in input.chars() {
        // match c {
        //     '^' => { let mut y = pos.remove(1); y += 1; pos.insert(1, y) },
        //     'v' => { let mut y = pos.remove(1); y -= 1; pos.insert(1, y) },
        //     '<' => { let mut x = pos.remove(0); x -= 1; pos.insert(0, x) },
        //     '>' => { let mut x = pos.remove(0); },
        //     _ => unreachable!(),
        // }
        visited.insert(&pos);
    }

    visited.len().to_string()
}

pub fn part2() -> String {
    let _input = parse_input("./input/2015/day03.txt");
    
    String::new()
}
