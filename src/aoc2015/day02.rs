

fn parse_input(input_str: &str) -> Vec<Vec<u32>> {
    let input = std::fs::read_to_string(input_str)
        .expect("Can't read input file");
    input
        .lines()
        .map(|l| {
            let len = l.split('x').nth(0).unwrap().parse::<u32>().unwrap();
            let wid = l.split('x').nth(1).unwrap().parse::<u32>().unwrap();
            let hei = l.split('x').nth(2).unwrap().parse::<u32>().unwrap();
            vec![len, wid, hei]
        }).collect()
}

pub fn part1() -> String {
    let input = parse_input("./input/2015/day02.txt");
    input
        .iter()
        .map(|v| {
            let x = *v.get(0).unwrap();
            let y = *v.get(1).unwrap();
            let z = *v.get(2).unwrap();
            if x*y <= y*z && x*y <= x*z {
                3*x*y + 2*y*z + 2*x*z
            } else if x*z <= x*y && x*z <= y*z {
                2*x*y + 3*x*z + 2*y*z
            } else if y*z <= x*y && y*z <= x*z {
                2*x*y + 2*x*z + 3*y*z
            } else { unreachable!() }
        }).sum::<u32>().to_string()
}

pub fn part2() -> String {
    let input = parse_input("./input/2015/day02.txt");
    input
        .iter()
        .map(|v| {
            let x = *v.get(0).unwrap();
            let y = *v.get(1).unwrap();
            let z = *v.get(2).unwrap();
            if x >= y && x >= z {
                x*y*z + 2*y + 2*z
            } else if y >= x && y >= z {
                2*x + x*y*z + 2*z
            } else if z >= x && z >= y {
                2*x + 2*y + x*y*z
            } else { unreachable!() }
        }).sum::<u32>().to_string()
}
